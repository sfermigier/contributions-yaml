# Open software base contributions

You can contribute to the [open software base](https://git.framasoft.org/codegouv/open-software-base-yaml) by sending merge request to this repository.

The contribution should be under the form of a YAML file with at least (but not limited to) the following fields:

```yaml
name: 'Name of the software'
description:
    en: 'Description in English'
    fr: 'Description en français'
```

Validity of the schema of contributions is tested by [the contribution validator](https://git.framasoft.org/codegouv/contributions-validator). You can see results of each merge request and pull at [https://validator.code.gouv2.fr/](https://validator.code.gouv2.fr/).
